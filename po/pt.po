# Portuguese translation for gnome-sound-recorder.
# Copyright © 2014 gnome-sound-recorder
# This file is distributed under the same license as the gnome-sound-recorder package.
# Tiago Santos <tiagofsantos81@sapo.pt>, 2014 - 2016.
# Sérgio Cardeira <cardeira dot sergio at gmail dot com>, 2016.
# Juliano de Souza Camargo <julianosc@protonmail.com>, 2020.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-sound-recorder master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-sound-recorder/"
"issues\n"
"POT-Creation-Date: 2021-05-13 17:45+0000\n"
"PO-Revision-Date: 2021-09-13 17:38+0100\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Portuguese (https://l10n.gnome.org/teams/pt/)\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0\n"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:5
#: data/org.gnome.SoundRecorder.desktop.in.in:4 data/ui/window.ui:28
#: src/application.js:32
msgid "Sound Recorder"
msgstr "Gravador de som"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:6
msgid "A simple, modern sound recorder for GNOME"
msgstr "Um simples e moderno gravador de som para o GNOME"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:10
msgid ""
"Sound Recorder provides a simple and modern interface that provides a "
"straight-forward way to record and play audio. It allows you to do basic "
"editing, and create voice memos."
msgstr ""
"O Gravador de som fornece um ambiente simples e moderno e uma forma avançada "
"de gravar e reproduzir áudio. Permite-lhe fazer edição básica e criar "
"memorandos de voz."

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:15
msgid ""
"Sound Recorder automatically handles the saving process so that you do not "
"need to worry about accidentally discarding the previous recording."
msgstr ""
"O Gravador de som gere automaticamente o processo de gravação para que não "
"necessite de se preocupar com acidentes com gravações anteriores."

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:19
msgid "Supported audio formats:"
msgstr "Formatos de áudio suportados:"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:21
msgid "Ogg Vorbis, Opus, FLAC, MP3 and MOV"
msgstr "Ogg Vorbis, Opus, FLAC, MP3 e MOV"

#: data/appdata/org.gnome.SoundRecorder.metainfo.xml.in.in:95
msgid "The GNOME Project"
msgstr "Projeto GNOME"

#: data/org.gnome.SoundRecorder.desktop.in.in:5
msgid "Record sound via the microphone and play it back"
msgstr "Grave som com o microfone e reproduza-o"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.SoundRecorder.desktop.in.in:10
msgid "Audio;Application;Record;"
msgstr "Áudio;Aplicação;Gravador;Sons;Som;"

#: data/org.gnome.SoundRecorder.gschema.xml.in:15
msgid "Window size"
msgstr "Tamanho da janela"

#: data/org.gnome.SoundRecorder.gschema.xml.in:16
msgid "Window size (width and height)."
msgstr "Tamanho da janela (largura e altura)."

#: data/org.gnome.SoundRecorder.gschema.xml.in:20
msgid "Window position"
msgstr "Posição da janela"

#: data/org.gnome.SoundRecorder.gschema.xml.in:21
msgid "Window position (x and y)."
msgstr "Posição da janela (x e y)."

#: data/org.gnome.SoundRecorder.gschema.xml.in:25
msgid "Maps media types to audio encoder preset names."
msgstr ""
"Mapeia tipos de multimédia para nomes predefinidos de codificador de áudio."

#: data/org.gnome.SoundRecorder.gschema.xml.in:26
msgid ""
"Maps media types to audio encoder preset names. If there is no mapping set, "
"the default encoder settings will be used."
msgstr ""
"Mapeia tipos de média de nomes predefinidos de codificação de áudio. Se não "
"houver mapeamento algum para um tipo de média, as configurações predefinidas "
"de codificação serão utilizadas."

#: data/org.gnome.SoundRecorder.gschema.xml.in:30
msgid "Available channels"
msgstr "Canais disponíveis"

#: data/org.gnome.SoundRecorder.gschema.xml.in:31
msgid ""
"Maps available channels. If there is not no mapping set, stereo channel will "
"be used by default."
msgstr ""
"Definir canais disponíveis. Se não estiver nenhum canal definido, o canal "
"estéreo será usado como predefinição."

#: data/ui/help-overlay.ui:15
msgctxt "shortcut window"
msgid "General"
msgstr "Geral"

#: data/ui/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Record"
msgstr "Gravar"

#: data/ui/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Stop Recording"
msgstr "Parar a gravação"

#: data/ui/help-overlay.ui:34
msgctxt "shortcut window"
msgid "Play / Pause / Resume"
msgstr "Reproduzir / Pausa / Retomar"

#: data/ui/help-overlay.ui:42
msgctxt "shortcut window"
msgid "Delete"
msgstr "Eliminar"

#: data/ui/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Abrir o menu"

#: data/ui/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Teclas de atalho"

#: data/ui/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sair"

#: data/ui/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Recording"
msgstr "Gravações"

#: data/ui/help-overlay.ui:76
msgctxt "shortcut window"
msgid "Seek Backward"
msgstr "Busca reversa"

#: data/ui/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Seek Forward"
msgstr "Busca à frente"

#: data/ui/help-overlay.ui:90
msgctxt "shortcut window"
msgid "Rename"
msgstr "Mudar o nome"

#: data/ui/help-overlay.ui:97
msgctxt "shortcut window"
msgid "Export"
msgstr "Exportar"

#: data/ui/recorder.ui:72
msgid "Resume Recording"
msgstr "Retomar a gravação"

#: data/ui/recorder.ui:96
msgid "Pause Recording"
msgstr "Pausar a gravação"

#: data/ui/recorder.ui:128
msgid "Stop Recording"
msgstr "Parar a gravação"

#: data/ui/recorder.ui:157
msgid "Delete Recording"
msgstr "Eliminar a gravação"

#: data/ui/row.ui:165 data/ui/row.ui:444
msgid "Export"
msgstr "Exportar"

#: data/ui/row.ui:189 data/ui/row.ui:440
msgid "Rename"
msgstr "Renomear"

#: data/ui/row.ui:241
msgid "Save"
msgstr "Gravar"

#: data/ui/row.ui:278 src/recorderWidget.js:100
msgid "Delete"
msgstr "Eliminar"

#: data/ui/row.ui:313
msgid "Seek 10s Backward"
msgstr "Retroceder 10s"

#: data/ui/row.ui:341
msgid "Play"
msgstr "Reproduzir"

#: data/ui/row.ui:367
msgid "Pause"
msgstr "Pausa"

#: data/ui/row.ui:399
msgid "Seek 10s Forward"
msgstr "Avançar 10s"

#: data/ui/window.ui:60
msgid "Record"
msgstr "Gravar"

#: data/ui/window.ui:123
msgid "Add Recordings"
msgstr "Adicionar gravações"

#: data/ui/window.ui:124
msgid "Use the <b>Record</b> button to make sound recordings"
msgstr "Utilize o botão <b>Gravar</b> para fazer gravações de sons"

#: data/ui/window.ui:181
msgid "Undo"
msgstr "Anular"

#: data/ui/window.ui:198
msgid "Close"
msgstr "Fechar"

#: data/ui/window.ui:234
msgid "Preferred Format"
msgstr "Formato preferido"

#: data/ui/window.ui:236
msgid "Vorbis"
msgstr "Vorbis"

#: data/ui/window.ui:241
msgid "Opus"
msgstr "Opus"

#: data/ui/window.ui:246
msgid "FLAC"
msgstr "FLAC"

#: data/ui/window.ui:251
msgid "MP3"
msgstr "MP3"

#: data/ui/window.ui:257
msgid "Audio Channel"
msgstr "Canais de áudio"

#: data/ui/window.ui:259
msgid "Stereo"
msgstr "Estéreo"

#: data/ui/window.ui:264
msgid "Mono"
msgstr "Mono"

#: data/ui/window.ui:271
msgid "_Keyboard Shortcuts"
msgstr "_Teclas de atalho"

#: data/ui/window.ui:276
msgid "About Sound Recorder"
msgstr "Acerca do Gravador de som"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/application.js:130
msgid "translator-credits"
msgstr ""
"Tiago Santos <tiagofsantos81@sapo.pt>\n"
"Pedro Albuquerque <palbuquerque73@gmail.com>\n"
"Juliano de Souza Camargo <julianosc@pm.me>\n"
"Hugo Carvalho <hugokarvalho@hotmail.com>"

#: src/application.js:132
msgid "A Sound Recording Application for GNOME"
msgstr "Um gravador de som para GNOME"

#. Translators: ""Recording %d"" is the default name assigned to a file created
#. by the application (for example, "Recording 1").
#: src/recorder.js:113
#, javascript-format
msgid "Recording %d"
msgstr "Gravação %d"

#: src/recorderWidget.js:94
msgid "Delete recording?"
msgstr "Eliminar a gravação?"

#: src/recorderWidget.js:95
msgid "This recording will not be saved."
msgstr "Esta gravação não será guardada."

#: src/recorderWidget.js:99
msgid "Resume"
msgstr "Retomar"

#. Necessary code to move old recordings into the new location for few releases
#. FIXME: Remove by 3.40/3.42
#: src/recordingList.js:42
msgid "Recordings"
msgstr "Gravações"

#. Translators: ""%s (Old)"" is the new name assigned to a file moved from
#. the old recordings location
#: src/recordingList.js:59
#, javascript-format
msgid "%s (Old)"
msgstr "%s (Antiga)"

#: src/row.js:71
msgid "Export Recording"
msgstr "Exportar gravações"

#: src/row.js:71
msgid "_Export"
msgstr "_Exportar"

#: src/row.js:71
msgid "_Cancel"
msgstr "_Cancelar"

#: src/utils.js:48
msgid "Yesterday"
msgstr "Ontem"

#: src/utils.js:50
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "há %d dia atrás"
msgstr[1] "há %d dias atrás"

#: src/utils.js:52
msgid "Last week"
msgstr "Última semana"

#: src/utils.js:54
#, javascript-format
msgid "%d week ago"
msgid_plural "%d weeks ago"
msgstr[0] "há %d semana atrás"
msgstr[1] "há %d semanas atrás"

#: src/utils.js:56
msgid "Last month"
msgstr "Último mês"

#: src/utils.js:58
#, javascript-format
msgid "%d month ago"
msgid_plural "%d months ago"
msgstr[0] "há %d mês atrás"
msgstr[1] "há %d meses atrás"

#: src/utils.js:60
msgid "Last year"
msgstr "Último ano"

#: src/utils.js:62
#, javascript-format
msgid "%d year ago"
msgid_plural "%d years ago"
msgstr[0] "há %d ano atrás"
msgstr[1] "há %d anos atrás"

#: src/window.js:71
#, javascript-format
msgid "\"%s\" deleted"
msgstr "“%s” eliminada"

#~ msgid "Microphone volume level"
#~ msgstr "Nível de volume do microfone"

#~ msgid "Microphone volume level."
#~ msgstr "Nível de volume do microfone."

#~ msgid "Speaker volume level"
#~ msgstr "Nível de volume das colunas"

#~ msgid "Speaker volume level."
#~ msgstr "Nível de volume das colunas."

#~ msgid "SoundRecorder"
#~ msgstr "Gravador de som"

#~ msgid "Preferences"
#~ msgstr "Preferências"

#~ msgid "About"
#~ msgstr "Sobre"

#~ msgid "Sound Recorder started"
#~ msgstr "O gravador de som foi iniciado"

#~ msgid "Info"
#~ msgstr "Informação"

#~ msgid "Done"
#~ msgstr "Terminado"

#~ msgctxt "File Name"
#~ msgid "Name"
#~ msgstr "Nome"

#~ msgid "Source"
#~ msgstr "Fonte"

#~ msgid "Date Modified"
#~ msgstr "Última alteração"

#~ msgid "Date Created"
#~ msgstr "Data de criação"

#~ msgctxt "Media Type"
#~ msgid "Type"
#~ msgstr "Tipo"

#~ msgid "Unknown"
#~ msgstr "Desconhecido"

#~ msgid "Recording…"
#~ msgstr "A gravar…"

#~ msgid "%d Recorded Sound"
#~ msgid_plural "%d Recorded Sounds"
#~ msgstr[0] "%d Som gravado"
#~ msgstr[1] "%d Sons gravados"

#~ msgid "MOV"
#~ msgstr "MOV"

#~ msgid "Load More"
#~ msgstr "Carregar mais"

#~ msgid "Default mode"
#~ msgstr "Modo predefinido"

#~ msgid "Volume"
#~ msgstr "Volume"

#~ msgid "Microphone"
#~ msgstr "Microfone"

#~ msgid "Unable to create Recordings directory."
#~ msgstr "Não foi possível criar a pasta de gravações."

#~ msgid "Please install the GStreamer 1.0 PulseAudio plugin."
#~ msgstr "Por favor, instale o complemento GStreamer 1.0 PulseAudio."

#~ msgid "Your audio capture settings are invalid."
#~ msgstr "As suas configurações de captura de áudio são inválidas."

#~ msgid "Not all elements could be created."
#~ msgstr "Nem todos os elementos puderam ser criados."

#~ msgid "Not all of the elements were linked."
#~ msgstr "Nem todos os elementos foram associados."

#~ msgid "No Media Profile was set."
#~ msgstr "Nenhum perfil multimédia foi definido."

#~ msgid ""
#~ "Unable to set the pipeline \n"
#~ " to the recording state."
#~ msgstr ""
#~ "Não foi possível definir o canal \n"
#~ " para o estado de gravação"

#~ msgid "Clip %d"
#~ msgstr "Excerto %d"

#~ msgid "%Y-%m-%d %H:%M:%S"
#~ msgstr "%Y-%m-%d %H:%M:%S"

#~ msgid "Preferred media type for encoding audio when recording"
#~ msgstr "Tipo de média preferido para codificação de áudio quando gravar"

#~ msgid ""
#~ "Preferred media type for encoding audio when recording. 'audio/x-vorbis' "
#~ "for Ogg Vorbis, or 'audio/mpeg' for MP3, for example. This is not a MIME "
#~ "type."
#~ msgstr ""
#~ "Tipo de média preferida para codificação de áudio quando gravar. \"audio/"
#~ "x-vorbis\" para Ogg Vorbis ou \"audio/mpeg\" para MP3, por exemplo. Isso "
#~ "não é um tipo MIME."

#~ msgid "Finish"
#~ msgstr "Concluir"

#~ msgid "Audio from %Y-%m-%d %H:%M:%S"
#~ msgstr "Áudio de %d-%m-%Y %H:%M:%S"

#~ msgid "Unable to set the playbin to the playing state."
#~ msgstr ""
#~ "Não foi possível definir o executável de reprodução para o estado de em "
#~ "reprodução."

#~ msgid "Audio from"
#~ msgstr "Áudio de"

#~ msgid "Error: "
#~ msgstr "Erro: "
